$(document).ready(function() {
    var html = '';
    //$("#bookbtnajax").click(function(){
        if (html == '') {
            $.ajax({
                type: 'GET',
                url: '/daftar',
                data: { get_param: 'value' },
                dataType: 'json',
                success: function (data) {

                  console.log(data);
                  for (var i = 0; i < data.items.length; i++) {
                        html += '<tr><td>' +
                        '<i onclick="fav(this)" class="far fa-star"></i></td><td>' +
                         data.items[i].volumeInfo.title + '</td><td>' +
                         data.items[i].volumeInfo.authors + '</td><td>' +
                         '<img src=' + data.items[i].volumeInfo.imageLinks.thumbnail + '></td></tr>'
                    }
                    $('#books').append(html);
                }
            });
        }
    //});
});

function fav(x) {
    $(x).toggleClass("far");
    $(x).toggleClass("fas");
    if ($(x).hasClass("far")) {
        var old = parseInt($('#counter_fav').text());
        $('#counter_fav').text(old - 1);
    } else {
        var old = parseInt($('#counter_fav').text());
        $('#counter_fav').text(old + 1);
    }
}
