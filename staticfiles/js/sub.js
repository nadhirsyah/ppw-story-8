var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $(".emailClass").keyup(function() {
        checkEmail();
    });

    $(".passwordClass").keyup(function() {
        $('.statusForm').html('');
        if ($('.passwordClass').val().length < 8) {
            $('.statusForm').append('<small style="color: black"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function() {
        checkEmail()
    });

    $('#submit').click(function () {
        data = {
            'name' : $('.nameClass').val(),
            'email' : $('.emailClass').val(),
            'password' : $('.passwordClass').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : '/add_subscribe/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('nameClass').value("");
                document.getElementById('emailClass').value("");
                document.getElementById('passwordClass').value("");

                $('.statusForm').html('');
                checkAll();
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('.emailClass').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    console.log('1');
    $.ajax({
        type: "POST",
        url: '/email_check/',
        data: data,
        dataType: 'json',
        success: function(data) {
          console.log('asdk');
            $('.statusForm').html('');
            if (data['status'] === 'fail') {
                console.log('aku');
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('.statusForm').append('<small style="color:black">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('.statusForm').append('<small style="color:white">' + data["message"] + '</small>');
            }

        }
    });
}

function checkAll() {
    if (emailIsValid &&
        $('.nameClass').val() !== '' &&
        $('.passwordClass').val() !== '' &&
        $('.passwordClass').val().length > 7) {

        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}
function deleteSubscriber(id){
    var email = $('#email-'+id).val();
    var password = prompt("Please enter your password","password")
    console.log($('#email-'+id).val());
    console.log(password)

    $.ajax({
        url: "/del_subscriber/",
        type: "POST",
        data: {
            pk: id,
            csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        success: function(result) {
          alert(response.message)
            console.log($(this));
             $('#subscriber').html("");
            getSubscriber();
           alert("Unsubscribed");
        },
        error : function (error){
            alert("Something is wrong");
        }
    });
};

function getSubscriber(){
     $.ajax({
        url: "/get_subscriber/",
        type: "GET",
        success: function(result){
            console.log(result[0]);
            for(i=0; i< result.length;i++){
                $('#subscriber').append("<div class='row'style='padding-bottom:10px;'><div class='col-sm-6'>"+result[i]['fields']['email']+"</div><div class='col-sm-6'><button onclick='deleteSubscriber("+
                                        result[i]['pk']+
                                        ")' class='btn btn-danger'"+">Unsubscribe</button></div></div>");
            }

        },
        error: function(errmsg){
            alert("error");
        }
    });
};

$(document).ready(function(){
   getSubscriber();
});
