$(document).ready(function(){
    $( "#accordion" ).accordion({
      active:false,
      collapsible:true
    });
    var changed = true;
    $("#themes").click(function(){
        if(changed){
            console.log("theme changed");
            $("body").css('background', '#303952');
            changed = false;
        }else{
            console.log("back to original");
            $("body").css('background', '#d1d8e0');
            changed = true;
        }

    });
});
