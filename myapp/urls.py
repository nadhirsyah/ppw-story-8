from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from django.urls import path
from . import views

urlpatterns = [
    path('daftar/', views.daftar, name = "daftar"),
    path('buku/', views.book, name='buku'),
    path('about/', views.about,  name="about"),
    path('login/',  views.login, name = "login"),
    path('logout/',  views.keluar, name = "logout"),
    path('like/', views.like, name = "like"),
    path('unlike', views.unlike, name = "unlike"),
    path("get_like", views.get_like, name = "get_like"),
    path('auth/', include('social_django.urls', namespace='social')),
    path('', views.home , name = 'home')
]
