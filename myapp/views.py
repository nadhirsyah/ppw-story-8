from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .forms import *
from .models import *
from django.core.validators import validate_email
from django.core import serializers
from django.contrib.auth import logout
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import urllib.request, json
import requests

response = {}

def about(request):
    return render(request, "about.html")

def book(request):
    return render(request, 'daftarbuku.html')

def daftar(request):
    URL = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
    req = requests.get(url = URL)
    data = req.json()
    return JsonResponse(data)

def subscribe_func(request):
    response = {'form':SubscribeForm}
    html = "subs.html"
    return render(request, html, response)

def email_validate(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Wrong Email Format'
        })
    found = Subscribe.objects.filter(email=request.POST['email'])
    if found:
        return JsonResponse({
            'message':'Email already exist',
            'status':'fail'
        })
    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })

def subscriber_add(request):
    if (request.method == "POST"):
        subscribe = Subscribe(
            name = request.POST['name'],
            email = request.POST['email'],
            password = request.POST['password']
        )
        subscribe.save()
        return JsonResponse({
            'message':'You have Subscribed'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"Fail!"
        }, status = 403)

def get_subscriber(request):
    if request.method == 'GET':
        lst_subscriber = serializers.serialize('json', Subscribe.objects.all())
        response = lst_subscriber

    else:
        response['message'] = 'Method POST not ALLOWED HERE'
    return HttpResponse(response, content_type='application/json')

def del_subscriber(request):
    if request.method == 'POST':
        subscriber = Subscribe.objects.get(pk=request.POST.get('pk'))
        print(subscriber)
        if password == subscriber[0].password:
            string = "Deleted " + obj[0].name
            subscriber.delete()
            response['message'] = "success"
            data = list(Subscribe.objects.values())
            response['data'] = data

        else:
            string = "Wrong password"
            response['message'] = string
    else:
        response['message'] = "Method GET not allowed here"
    return JsonResponse(response)

def home(request):
	return render(request, 'daftarbuku.html')

def login(request):
    return render(request, 'login.html')

def keluar(request):
    logout(request)
    return redirect('buku')

def books(request, search='quilting'):
    if request.user.is_authenticated and 'like' not in request.session:
        request.session["fullname"]=request.user.first_name+" "+request.user.last_name
        request.session["username"]=request.user.username
        request.session["email"] = request.user.email
        request.session["sessionid"]=request.session.session_key
        request.session["like"]=[]
    return render(request, 'daftarbuku.html', {'search':search})

@csrf_exempt
def like(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] not in lst :
            lst.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")


@csrf_exempt
def unlike(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] in lst :
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

def get_like(request):
    if request.user.is_authenticated :
        if(request.method=="GET"):
            if request.session["like"] is not None:
                response["message"] = request.session["like"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)
