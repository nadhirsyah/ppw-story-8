from django import forms

class SubscribeForm(forms.Form):
    name = forms.CharField(label="Name", max_length = 140, widget = forms.TextInput(
        attrs = {
            'class':'col-md-12 nameClass form-control form-control',
            'placeholder':'Name',
            'required':True,
        }
    ))
    email = forms.EmailField(label="Email", max_length = 140, widget = forms.TextInput(
        attrs = {
            'class':'col-md-12 emailClass form-control',
            'placeholder':'Email',
            'required':True,
        }
    ))
    password = forms.CharField(label="Password", max_length = 140, widget = forms.TextInput(
        attrs = {
            'class':'col-md-12 passwordClass form-control',
            'placeholder':'Password',
            'required':True,
            'type':'password'
        }
    ))
