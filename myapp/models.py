from django.db import models

class Subscribe(models.Model):
    name = models.CharField(max_length = 255)
    email = models.EmailField(max_length = 255)
    password = models.CharField(max_length = 255)
# Create your models here.
