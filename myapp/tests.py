from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from myapp.views import *
from myapp.apps import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    #def test_list_url_is_exist(self):
    #    response = Client().get('/buku')
    #    self.assertEqual(response.status_code, 200)

    #def test_index_contains_greeting(self):
    #    response = Client().get('')
    #    response_content = response.content.decode('utf-8')
    #    self.assertIn("Muhammad Nadhirsyah Indra", response_content)

    #def test_url_books_api(self):
    #    response = Client().get('/daftar')
    #       self.assertEqual(response.status_code, 200)

    #def test_view_books_api(self):
    #    found = resolve('/daftar')
    #    self.assertEqual(found.func, daftar)

    #def test_json_data(self):
    #    response = Client().get('/daftar').json()
    #    data = response['kind']
    #    self.assertIn('books#volumes', data)

    def test_using_check_email(self):
        found = resolve('/email_check/')
        self.assertEqual(found.func, email_validate)

    def test_using_subscribe_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe_func)

    def test_using_subscribe_add(self):
        found = resolve('/add_subscribe/')
        self.assertEqual(found.func, subscriber_add)

    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest,self).tearDown()



    '''def test_change(self):
        self.browser.get('http:/nadhirsyah-story4.herokuapp.com/')
        button = self.browser.find_element_by_id('themes')
        body = self.browser.find_element_by_css_selector('body')
        color = body.value_of_css_property('background')
        self.assertEqual("rgb(209, 216, 224) none repeat scroll 0% 0% / auto padding-box border-box", color)
        button.send_keys(Keys.ENTER)
        color2 = body.value_of_css_property('background')
        self.assertEqual("rgb(155, 89, 182) none repeat scroll 0% 0% / auto padding-box border-box", color2)

    def test_accordion(self):
        self.browser.get('http:/nadhirsyah-story4.herokuapp.com/')
        accordion = self.browser.find_elements_by_id('accd')
        text = accordion[0].text
        self.assertIn("Aktivitas", text)
        organization = accordion[1]
        text2 = organization.text
        self.assertIn("Organisasi", text2)
        organization.send_keys(Keys.ENTER)
        list = self.browser.find_elements_by_tag_name('li')'''
